import 'package:flutter/material.dart';
import 'package:flutter_api/my_home_page.dart';
import 'package:flutter_api/pages/login_screen.dart';
import 'package:flutter_api/services/auth.dart';
import 'package:provider/provider.dart';
import 'models/preferences.dart';
import 'models/recipe_list.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  /*
  Séquence d'initialisation
   */
  await beforeLaunch();

  // Lancement du projet
  runApp(
    /// Providers are above [MyApp] instead of inside it, so that tests
    /// can use [MyApp] while mocking the providers
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => Auth()),
      ],
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Larecette'),
      /*initialRoute: Preferences.getLogin(),
        routes: <String, WidgetBuilder>{
          '/': (BuildContext context) => MyHomePage(title: "Larecette"),
          '/login': (BuildContext context) => LoginScreen(),
        } */
    );
  }

}

/*
Insérer le code de l'initialisation ici (ex. geolocalisation, BDD, etc...)
 */
Future beforeLaunch() async {
  await Preferences.init();
}

