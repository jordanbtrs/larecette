import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../constant.dart';
import '../models/preferences.dart';
import '../models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Auth extends ChangeNotifier {


  late http.Response responseCreds;

  static bool errorApi = false;
  static bool _isLoggedIn = false;
  static late User _user;
  late String _token;
  bool get authenticated => _isLoggedIn;
  User get user => _user;


  void login({required Map creds}) async {
    //print(creds["email"]);

    try {
      http.Response responseCreds = await http.post(Uri.parse('${Constant.url}/api/login'), headers: {'Content-Type':'application/json'}, body: jsonEncode(creds));
      var responseJson = jsonDecode(responseCreds.body);
     print(responseCreds.statusCode);

     if(responseCreds.statusCode == 200) {
       errorApi = false;
       print(responseCreds.body);
       //print(responseCreds.body);
       // print(json);
       // print(json["token"]);
       String token = responseJson["token"];
       // print(token);
       tryToken(token: token);
       _isLoggedIn = true;
       //Preferences.setLogin(token, _isLoggedIn);
       Preferences.setLogin(token, authenticated);
       notifyListeners();
     } else if (responseCreds.statusCode == 401) {
       errorApi = true;
       print("acces refuser");
       notifyListeners();
     }

    } catch (e) {
      print("nonononon");

    }

  }

  void tryToken({String? token}) async {
    if (token == null) {
      return;
    } else {
      try{
        http.Response response = await http.get(Uri.parse('${Constant.url}/api/user'), headers: {'Content-Type':'application/json', 'Authorization' : 'Bearer $token'});
        var repJson = jsonDecode(response.body);

        if ( response.statusCode == 200) {
          _user = User.fromJson(repJson);
          _token = token;
          _isLoggedIn = true;
          notifyListeners();
        } else {
          print("error login");
        }



      } catch (e) {
        print(e);
      }
    }
  }

  void logout() async {
  try {
    http.Response response = await http.get(Uri.parse('${Constant.url}/api/user/logout'), headers: {'Content-Type':'application/json', 'Authorization' : 'Bearer $_token'});
    //cleanUp();
    await Preferences.prefs?.clear();
    Preferences.clearBool();
    //print(response.body);
  } catch (e) {
   // print(e);
    }
    //print(Preferences.prefs?.getBool('login').toString());
    _isLoggedIn = false;
    notifyListeners();
  }
}

/*

recup le code status, afficher error si login faux

gérer le try token si user supprimer

 */