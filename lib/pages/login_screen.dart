import 'package:flutter/material.dart';
import 'package:flutter_api/models/preferences.dart';
import 'package:flutter_api/my_home_page.dart';
import 'package:provider/provider.dart';
import '../main.dart';
import '../services/auth.dart';
import 'package:shared_preferences/shared_preferences.dart';


class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {

  SharedPreferences? localStorage;

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    // TODO: implement initState

    _emailController.text = '';
    _passwordController.text = '';
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
        title: Text('Liste des recettes'),
    actions: [
    IconButton(
    icon: Icon(Icons.refresh),
    onPressed: () {
      Navigator.of(context).push(MaterialPageRoute(builder: (
          context) => MyHomePage(title: "Liste des recettes",)));
      },
    )
    ],
        ),
        body: Padding(
          padding: const EdgeInsets.all(20.0),
        child: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextFormField(
                decoration: InputDecoration(
                    hintText: "Email"
                ),
                controller: _emailController,
                validator: (value) => value!.isEmpty ? 'please enter valid email' : null
            ),
            TextFormField(
                decoration: InputDecoration(
                    hintText: "password"
                ),
                obscureText: true,
                controller: _passwordController,
                validator: (value) => value!.isEmpty ? 'please enter password' : null
            ),
            ElevatedButton(
              onPressed: () async {
                Map creds = {
                  'email' : _emailController.text,
                  'password' : _passwordController.text,
                  'device_name': 'mobile',
                };
                if (_formKey.currentState!.validate()) {
                 Provider.of<Auth>(context, listen: false)
                      .login(creds: creds);
                  if(Auth.errorApi == false) {
                    Navigator.pop(context);
                    Auth.errorApi = false;
                  } else if (Auth.errorApi == true) {
                  ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        content: Container(
                          decoration: BoxDecoration(
                            color: Colors.red,
                            borderRadius: BorderRadius.all(Radius.circular(20)),
                          ),
                          child: Text("Identifiants incorrect"),
                        ),
                        behavior: SnackBarBehavior.floating,
                        backgroundColor: Colors.red,
                        elevation: 99,
                      )
                  );
                  }
                } else {}
              },
              child: Text('Login'),
            ),
          ],
        )
    ),
  ),
    );
  }
}
