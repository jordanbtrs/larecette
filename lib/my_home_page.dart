import 'package:flutter/material.dart';
import 'package:flutter_api/models/api_recipe.dart';
import 'package:flutter_api/models/preferences.dart';
import 'package:flutter_api/models/recipe.dart';
import 'package:flutter_api/models/recipe_grid.dart';
import 'package:flutter_api/models/recipe_list.dart';
import 'package:flutter_api/models/recipe.dart';
import 'package:flutter_api/widgets/chargement.dart';
import 'package:flutter_api/widgets/erreur.dart';
import 'package:flutter_api/pages/login_screen.dart';
import 'package:flutter_api/services/auth.dart';
import 'package:provider/provider.dart';

import 'constant.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Recipe> recipes = [];
  late StatusApi _statusApi;

  @override
  void initState() {
    getPopularRecipes();
    super.initState();
  }

  // home screen

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Liste des recettes'),
        actions: [
          IconButton(
              icon: Icon(Icons.refresh),
              onPressed: getPopularRecipes,
          )
        ],
      ),
      body: choixDuBody(),
      drawer: Drawer(
          child: Consumer<Auth>(builder: (context, auth, child) {
            if (!auth.authenticated) {
                return ListView(
                  children: [
                    ListTile(
                      title: Text('login'),
                      leading: Icon(Icons.login),
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(builder: (
                            context) => LoginScreen()));
                      },
                    ),
                  ],
                );
            } else {
              return  ListView(
                children: [
                  DrawerHeader(
                    decoration: BoxDecoration(
                        color: Colors.blue
                    ),
                    child: Column(
                      children: [
                        CircleAvatar(
                         // backgroundColor: Colors.white,
                          backgroundImage: NetworkImage("${Constant.url}${auth.user.avatar}"),
                          radius: 30,
                        ),
                        SizedBox(height: 10),
                        Text(
                          //"name",
                          auth.user.name,
                          style: TextStyle(color: Colors.white),
                        ),
                        SizedBox(height: 10),
                        Text(
                          auth.user.email,
                          style: TextStyle(color: Colors.white),
                        ),
                      ],
                    ),
                  ),
                  ListTile(
                    title: Text('logout'),
                    leading: Icon(Icons.logout),
                    onTap: () {
                      Provider.of<Auth>(context, listen: false)
                          .logout();
                    },
                  ),
                ],
              );
            }
          })
      ),
    );
  }
  //end home screen

  Widget choixDuBody(){
    if(_statusApi == StatusApi.chargement){
      return Chargement();
    }else if(_statusApi == StatusApi.error){
      return Erreur();
    }else{
      Orientation orientation = MediaQuery.of(context).orientation;
      if(orientation == Orientation.portrait){
        return RecipeList(recipes: recipes);
      }else{
        return RecipeGrid(recipes: recipes);
      }
    }
  }

  Future<void> getPopularRecipes() async {
    setState(() {
      _statusApi = StatusApi.chargement;
    });
    ApiRecipe api = ApiRecipe();
    Map<String, dynamic> mapRecipe = await api.getRecipes();
    if(mapRecipe["code"] == 0){
      // Je peux concevoir ma liste de recettes
      setState(() {
        recipes = Recipe.recipesFromApi(mapRecipe["body"]);
        _statusApi = StatusApi.ok;
      });
    }else{
      setState(() {
        _statusApi = _statusApi = StatusApi.error;
      });
    }
  }
}

enum StatusApi {
  chargement,
  error,
  ok
}