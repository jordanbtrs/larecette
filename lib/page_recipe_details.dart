import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_api/models/recipe.dart';


class PageRecipeDetails extends StatelessWidget {
  final Recipe recipe;
  PageRecipeDetails({Key? key, required this.recipe}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    final Size _size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: Text("Détail de la recette"),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(15),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text(recipe.recipeTitle, style: TextStyle(fontSize: 30)),
              Card(
                elevation: 8,
                child: Container(
                  width: _size.width / 1.1,
                  child: Image.network(
                    recipe.getImageUrl(),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              Text("ingredients", style: TextStyle(fontSize: 30),),

              for( int index = 0; index != recipe.ingredients.length; index++)
                Text("${recipe.ingredients[index].title} : ${recipe.ingredients[index].quantity}${recipe.ingredients[index].unit}",style: TextStyle(fontSize: 20)),
                Text("steps", style: TextStyle(fontSize: 30),),
              for( var i = 1; i <= recipe.steps.length; i++)
                Text("${recipe.steps["$i"]["key"]} : ${recipe.steps["$i"]["value"]}",style: TextStyle(fontSize: 20)),
            ],
          ),
        ),
      ),
    );
  }
}