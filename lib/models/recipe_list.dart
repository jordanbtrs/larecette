import 'package:flutter/material.dart';
import 'package:flutter_api/models/preferences.dart';
import 'package:flutter_api/models/recipe.dart';
import 'package:flutter_api/page_recipe_details.dart';

class RecipeList extends StatelessWidget {
  RecipeList({Key? key, required this.recipes}) : super(key: key);
  List<Recipe> recipes = [];

  @override
  Widget build(BuildContext context) {
    final Size _size = MediaQuery.of(context).size;

    return ListView.builder(
        itemCount:recipes.length,
        itemBuilder: (context, i){
          Recipe recipe = recipes[i];

          return Container(
            padding: EdgeInsets.all(10.0),
            child: Card(
                elevation: 10.0,
                // InkWell car on a besoin d'un onTap pour aller sur une autre vue
                child: InkWell(
                  onTap: (){
                    Navigator.push(
                        context,
                        MaterialPageRoute(builder: (BuildContext context){
                          return PageRecipeDetails(recipe: recipe);
                        })
                    );
                  },
                  child: Column(
                    children: <Widget>[
                      //Titre du film + date
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                         Text(recipe.recipeTitle, style: TextStyle(color: Colors.black),),
                          ],
                      ),
                       Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Card(
                            elevation: 7.5,
                            child: SizedBox(width: _size.width / 1.1, height: _size.height / 4,child: Image.network(recipe.getImageUrl(),fit: BoxFit.fitWidth)),
                          ),
                        ],
                      )
                    ],
                  ),
                )
            ),
          );
        });
  }
}
