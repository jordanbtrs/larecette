import 'dart:convert';
import 'package:flutter_api/constant.dart';
import 'package:http/http.dart' as http;

class ApiRecipe{
  final String url = "https://65d2-195-25-86-163.eu.ngrok.io";
  final String key = "49ecd4647f56c9ac5496c71a9cfe5ba9";

  // Méthode qui interroge les films populaire : /movie/popular
  // Cette méthode retourne un body JSON
  // PLusieurs cas de figure :
  //  - Le Body est ok
  //  - Le Body est vide
  //  - Le body n'a rien à voir (message d'erreur)

  // Notre méthode doit retourner 2 choses :
  //  - Le code HTTP ou code interne à vous
  //  - Le contenu (message erreur, body json etc.)
  // ça s'appelle une Map

  Future<Map<String, dynamic>> getRecipes() async{
    http.Response response;
    String completeUrl = "${Constant.url}/api/recipes";
    print(completeUrl);

    response = await http.get(Uri.parse(completeUrl));

    Map<String, dynamic> map = {
      "code":1,
      "body":""
    };

    if(response.statusCode==200){
      //todo tester les # cas de figure retournée par l'API
      map["code"] = 0;
      map["body"]= jsonDecode(response.body);
    }else{
      map["code"] = 1;
      map["body"]="Error http etc.";
    }
    print(map);
    return map;
  }

  String getUrl() {
    return url;
  }

}