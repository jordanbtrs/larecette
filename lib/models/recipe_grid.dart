import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_api/models/recipe.dart';

class RecipeGrid extends StatelessWidget {
  RecipeGrid({Key? key, required this.recipes}) : super(key: key);
  List<Recipe> recipes = [];

  @override
  Widget build(BuildContext context) {
    final Size _size = MediaQuery.of(context).size;
    print("Context Build =  ${_size.height}");
    return GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 4, childAspectRatio: 1),
        itemCount:recipes.length,
        itemBuilder: (context, i){
          final Size _GridViewsize = MediaQuery.of(context).size;
          print("Context Grid =  ${_GridViewsize.height}");
          Recipe recipe = recipes[i];
          double height = _size.width / 4 / 1.5;
          print("Height = $height");
          return GridTile(
            header: Text(recipe.recipeTitle),
            child:Container(
              padding: EdgeInsets.all(height * 0.2),
               child: Image.network(recipe.getImageUrl(), fit: BoxFit.cover,),
            ),
          );
        });
  }
}
