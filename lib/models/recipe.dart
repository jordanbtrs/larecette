import 'dart:convert';
import 'package:flutter_api/constant.dart';
import 'package:flutter_api/models/ingredient.dart';

import './api_recipe.dart';

class Recipe {
  final int recipeId;
  final String recipeTitle;
  final String recipeImage;
  final Map<String, dynamic> steps;
  //final List<dynamic> ingredients;
  final List<dynamic> ingredients;
  //final List<Ingredient> ingredientsList;

  Recipe({required this.recipeId, required this.recipeTitle, required this.recipeImage, required this.steps, required this.ingredients, /*required this.ingredientsList*/});



  static List<Recipe> recipesFromApi(Map<String, dynamic> body){
    List<Recipe> l =[];

    List<dynamic> results = body["results"];
    //print(results);

    results.forEach((value) {
      Recipe recipe = Recipe(
          recipeId: value["id"],
          recipeTitle: value["name"],
          recipeImage: value["image"],
          steps: value["steps"],
          ingredients: Ingredient.ingredientFromRecipe(value["ingredients"]),

      );
      //print(recipe.ingredients);
      l.add(recipe);
    });

    return l;
  }

   String getImageUrl(){
    print(recipeImage);
    return "${Constant.url}/$recipeImage";
  }
}