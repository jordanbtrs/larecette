class Ingredient {
  late final int id;
  late final String title;
  late final String image;
  late final String quantity;
  late final String unit;

  Ingredient({required this.id, required this.title, required this.image, required this.quantity, required this.unit});

  static List<Ingredient> ingredientFromRecipe(List<dynamic> body) {
    List<Ingredient> I =[];

    List<dynamic> results = body;
    results.forEach((value) {
      Ingredient ingredient =  Ingredient(
        id: value["id"],
        title: value["name"],
        image: value["image"],
        quantity: value["pivot"]["quantity"],
        unit: value["pivot"]["unit"],
      );

      I.add(ingredient);

    });
    print(I);
    return I;

  }

}