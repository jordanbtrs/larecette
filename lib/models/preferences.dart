import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_api/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../constant.dart';
import '../services/auth.dart';

import 'package:http/http.dart' as http;

class Preferences {
  static SharedPreferences? prefs;
  static bool? isLoggedIn;
  static String? email;
  static String? token;

  static Future init() async {
    prefs = await SharedPreferences.getInstance();
   isLoggedIn = prefs?.getBool('login');
    email = prefs?.getString('email');
    token = prefs?.getString('token');
    getLogin();
  }


  static void getHomepage() {
    getLogin();

  }

  // Enregistre le token et le boolean  login
  static setLogin(String token, bool login) async {
    Preferences.prefs?.setString('token', token);
    Preferences.prefs?.setBool('login', login);
  }

  //Récupère les données de l'utilisateur dans les données partagée
  static void getLogin() {
    if(prefs?.getString('token') != null) {
      Auth().tryToken(token: token);
    }
  }

  static String? printBool() {
    return prefs?.getBool('login').toString();
  }

  static clearBool() async {
    prefs?.setBool('login', false);
  }
}